##################
3: Send and import data to i-doit
##################

*********
Send data
*********

Go back to the home screen and click on "Sync" button.

.. image:: img/2_sync_overview.jpg
    :height: 600px
    :alt: alternate text
    :align: Scan View

Now click the "Send Data" button.

.. image:: img/6_sync_send_data.jpg
    :height: 600px
    :alt: alternate text
    :align: center

Select your files to send to i-doit. After selecting, click the "Send To I-Doit" button.


*********
Import data to i-doit
*********

Now go to the overview of ScanIT add-on in i-doit. Click the "Import" menu to display the files to be imported.

.. image:: img/idoit_import.png
    :alt: alternate text
    :align: center

After selecting a file, you get a table preview of the file.

.. image:: img/idoit_import2.png
    :alt: alternate text
    :align: center

If an error occured while opening a file, you will receive a response via the GUI

.. image:: img/idoit_import3.png
    :alt: alternate text
    :align: center

Use the "update" link in the row, to fix the problems via GUI. Or delete the row by clicking the "delete" link.

.. image:: img/idoit_import4.png
    :alt: alternate text
    :align: center

Click "save" after fixing the problems, to return to the table preview site.

.. image:: img/idoit_import2.png
    :alt: alternate text
    :align: center

Now select the lines to be imported. This concludes the process :-)


