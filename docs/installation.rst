##################
Installation
##################

The following describes the complete installation of the ScanIT add-on. First of all you need to install the ScanIT add-on into the i-doit System. After this step you can download the ScanIT app from `Andorid-Playstore`_.

.. _Andorid-Playstore: https://play.google.com/store/apps/details?id=becon.scanit

*********
i-doit
*********

Download
=========

Getting the ScanIT add-on from `becon`_.

.. _becon: mailto:info@becon.de

Installation
=========
The installation of the ScanIT add-on is installable via the i-doit Admin Center. The Admin Center is accessible via the addition / admin in the address bar of the browser.

.. image:: img/installation1.png
    :alt: alternate text
    :align: center

Then navigate to the add-ons tab. And select the button "Install / update add-on".

.. image:: img/installation2.png
    :alt: alternate text
    :align: center

Now the corresponding ZIP archive containing the add-on can be selected via "Browse" and then installed ("Update and install").
In this case, it is also important to note that if several clients are installed, for which tenants the add-on should be made available.

.. note::
    Maybe you need to add access permissions (see below) inside of .htaccess (e.g (/var/www/html/idoit/.htaccess)
   ## …except some PHP files in script/:
    <Files "dataTransfer.php">
        Require all granted
    </Files>


*********
Android
*********

Download
=========

Getting the app from the `android playstore`_.

.. _android playstore: https://play.google.com/store/apps/details?id=becon.scanit


Installation
=========

After installation you will see the home screen of this app.

.. image:: img/1_home.jpg
    :height: 600px
    :alt: home screen
    :align: center

