##################
Scan
##################

After the profiles have been imported from the CMDB into the application, you can start creating your records. Next to each text field where a "Scan" button exists, it is possible to scan a barcode. You do not need an internet connection for this process.

*********
Create new dataset
*********

Click on "Scan" in the home screen.

.. image:: img/scan/scan_select_profiles.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Select a profile in which the data for the dataset will be entered.
	
.. image:: img/scan/scan_select_overview.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Now you see your configured fields from i-doit system. Type into a row for entering datasets by keyboard.
	
.. image:: img/scan/scan_select_dropdown.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Via Device Type and CMDB status you have the possibility to select an attribute by preselection.

.. note::
    The default values of the dropdown lists can be adjusted in :doc:`Settings<../usage-settings>`.
	
.. image:: img/scan/scan_select_calendar.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
If a date is to be entered, this can be done conveniently via the calendar interface. Click on the "Calendar" button next to the text field.
	
.. image:: img/scan/scan_select_location.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
In the text field "Locations" an autocomplete appears after the first letter is entered to help you with the entry.

.. note::
    If no autocomplete appears, make sure that "Location.csv" file was loaded in :doc:`Sync<../usage-sync>`.
	
.. image:: img/scan/scan_select_complete.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
After all required values have been entered, the data set can be saved using the "Save" button.
	
.. image:: img/scan/scan_select_saved.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
A message appears that it has run successfully.
	
.. image:: img/scan/scan_select_datastore.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
To edit or delete the file, go to :doc:`Datastore<../usage-datastore>` and select "Local". Here you can see the created file. 

.. note::
    The filename is: "profilename_username_timestamp.csv". If more datasets are created with the same profile, username and on the same day, they will all be written to this one file. If the username is changed or the datasets are edited on a different day, a new file will be added automatically. The username can be changed or disabled in :doc:`Settings<../usage-settings>`.

------------

*********
Scan barcode
*********

To create datasets faster during execution, use the scan function to scan barcodes.

.. image:: img/scan/scan_select_overview.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
It is possible to scan everywhere where there is a "barcode" button next to the text field.

.. image:: img/scan/scan_select_scanbutton.jpg
    :alt: alternate text
    :align: center
	
Use the "barcode" button to open the barcode scanner.
	
.. image:: img/app_scan3.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Hold the mobile device to the barcode to capture it.

.. image:: img/scan/scan_select_scan.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
The entered data is transferred to the text field. Click on the "Save" button, to save the dataset.
