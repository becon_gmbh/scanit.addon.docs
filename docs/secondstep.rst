##################
2: Get profiles, lists and start scanning
##################

*********
Getting profiles
*********

Starting your android app. You will see four buttons.

.. image:: img/1_home.jpg
    :height: 600px
    :alt: alternate text
    :align: center

First of all you need to sync your profiles from i-doit. Click on "Sync".

.. note::
    At this part it is assumed that connector is already configured!

.. image:: img/2_sync_overview.jpg
    :height: 600px
    :alt: alternate text
    :align: center

Now you can see two options. "Get Data" and "Send Data". That means, if we want to get data from i-doit system or if we want to send data to i-doit system.
So we want to get the profiles :-)

.. image:: img/sync/sync_getdata_selectprofiles.jpg
    :height: 600px
    :alt: alternate text
    :align: center

Now choose the profiles under "Profiles from i-doit" you want to sync to your android app by using the checkboxes. Then click "Update" to sync the files.

------------

*********
Getting lists
*********

The second step before you can start scanning is to sync the lists. All available attributes of object types and cmdb statuses are fetched from i-doit and stored locally in the app. These are put in dropdown lists for preselection in the respective fields.

.. image:: img/2_1_dropdownlists.jpg
    :height: 600px
    :alt: alternate text
    :align: center

Choose "ObjectTypes.csv" and "CMDBStatus.csv" under "Dropdown lists". Then click "Update" to sync the files.

Go back to home screen and click on "Scan" button.

.. image:: img/2_2_dropdownlists.jpg
    :height: 600px
    :alt: alternate text
    :align: center

Choose one of your synchronized i-doit profiles.

------------

*********
Start scanning
*********

.. image:: img/5_scan_selected.jpg
    :height: 600px
    :alt: alternate text
    :align: center

Now you see your configured fields from i-doit system. Type into a row for entering datasets by keyboard. Use the "barcode" button to open the barcode scanner.

.. image:: img/app_scan3.jpg
    :height: 600px
    :alt: alternate text
    :align: center

Click on the "Save" button, to save the dataset.
