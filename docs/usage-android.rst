##################
Android
##################

The individual functions of the mobile application are explained in more detail here.

* :ref:`Sync Overview`
* :ref:`Scan Overview`
* :ref:`Datastore Overview`
* :ref:`Settings Overview`

.. _Sync Overview:

*********
Sync
*********

===========
Get data
===========

Get data means that individual information is obtained from the i-doit CMDB. These specify the way in which further details can be stored. Or this data can also be used as a tool to make work easier. These values must be obtained once, from then on further work is possible without an internet connection. A connection is only required to send the data packages back to the database or to use the online mode in datastore.

-----------
Profiles
-----------

Profiles are created in i-doit and specify to the user which attributes can be stored in the app and which the CMDB expects in the end. This is how the structure of the various forms is built. In conclusion, this means that at least one profile must be accessible on the mobile device so that datasets can be generated.

.. image:: img/sync/sync_getdata_idoitprofiles.jpg
    :alt: CMDB Config
    :align: center
	
Create your customized profiles in the i-doit ScanIT add-on under the menu item "Export".

.. image:: img/sync/sync_getdata_select.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
Click on "Sync" in the home screen and then on "Get Data".

.. note::
    A continuous internet connection is required for this and make sure that you have entered your i-doit url and apikey under :doc:`Settings<../usage-settings>`.
	
.. image:: img/sync/sync_getdata_selectprofiles.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
At "Profiles From I-Doit" all profiles are shown which are received from CMDB. Select the profiles you want to work with and click "Update". The checkbox in the header selects all files.

.. image:: img/sync/sync_getdata_selectprofiles_saved.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
A message appears that the profiles have been successfully stored in the local memory.

.. note::
    When you make changes to profiles in i-doit, don't forget to update the profiles within the app.

.. image:: img/sync/sync_getdata_selectprofiles_saved_select.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
These profiles can be selected immediately at various points to create datasets.
	
.. image:: img/sync/sync_getdata_selectprofiles_saved_selectprofile.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
The creation of the profiles in i-doit determines the structure of the forms and which values are expected.


------------

-----------
Lists 
-----------

Lists enable a better workflow during work. With the preloaded lists, dropdown lists or autocomplete text with attributes are provided for preselection. This allows you to create new datasets in a short time.

^^^^^^^^^
Object Types and CMDB Status
^^^^^^^^^

.. image:: img/sync/lists/sync_getdata_lists_select.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
Select the checkboxes "ObjectTypes.csv" and "CMDBStatus.csv" and load the file via "Update".
	
.. image:: img/sync/lists/sync_getdata_lists_saved.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
After successful download a message appears.
	
.. note::
    These two files are needed so that datasets can be created. If these values are changed in the i-doit CMDB, do not forget to reload the lists.
	
.. image:: img/sync/lists/sync_getdata_lists_cmdbstatus.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
.. image:: img/sync/lists/sync_getdata_lists_devicetype.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
In the forms, dropdown lists are provided for the selection of values from the CMDB. In this case for CMDB status and Device Type.

.. note::
    The value shown first can be changed as default value in :doc:`Settings<../usage-settings>`.

^^^^^^^^^
Locations
^^^^^^^^^

.. image:: img/sync/lists/sync_getdata_lists_select_location.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
The "Location.csv" can be obtained for input assistance in locations. 
	
.. image:: img/sync/lists/sync_getdata_lists_show_locations.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center

Press the text field for locations. As soon as letters are entered, an autocomplete is created for all location object types where these letters occur.

.. note::
    With "*" all available location object types are displayed. In :doc:`Settings<../usage-settings>` you can specify the ids of the location object types to be retrieved from CMDB.

------------

===========
Send data
===========

After the files with the different datasets are complete, they can be sent back to the CMDB. There a change owner has the possibility to check and accept the data.

Click on "Sync" in the home screen and then on "Send Data".

.. image:: img/sync/sync_senddata_select.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Select the files to be sent to the database. The checkbox in the header selects all files. Press "Send To I-Doit" to start the transmission.
	
.. image:: img/sync/sync_senddata_synced.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
It is displayed whether the respective file was successfully synchronized with the CMDB.
	
.. image:: img/sync/sync_senddata_exists.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
If there is a file with the same name in the CMDB import, it will be mentioned. 
	
.. image:: img/sync/sync_senddata_rest.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
The data sent to i-doit will be automatically removed from the device and from the list.
	
.. image:: img/sync/sync_senddata_idoit.jpg
    :alt: alternate text
    :align: center
	
After successful transfer, the data supplied can be processed by the change owner under "Import" in i-doit.

------------

------------

.. _Scan Overview:

*********
Scan
*********

After the profiles have been imported from the CMDB into the application, you can start creating your records. Next to each text field where a "Scan" button exists, it is possible to scan a barcode. You do not need an internet connection for this process.

===========
Create new dataset
===========

Click on "Scan" in the home screen.

.. image:: img/scan/scan_select_profiles.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Select a profile in which the data for the dataset will be entered.
	
.. image:: img/scan/scan_select_overview.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Now you see your configured fields from i-doit system. Type into a row for entering datasets by keyboard.
	
.. image:: img/scan/scan_select_dropdown.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Via Device Type and CMDB status you have the possibility to select an attribute by preselection.

.. note::
    The default values of the dropdown lists can be adjusted in :doc:`Settings<../usage-settings>`.
	
.. image:: img/scan/scan_select_calendar.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
If a date is to be entered, this can be done conveniently via the calendar interface. Click on the "Calendar" button next to the text field.
	
.. image:: img/scan/scan_select_location.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
In the text field "Locations" an autocomplete appears after the first letter is entered to help you with the entry.

.. note::
    If no autocomplete appears, make sure that "Location.csv" file was loaded in :doc:`Sync<../usage-sync>`.
	
.. image:: img/scan/scan_select_complete.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
After all required values have been entered, the data set can be saved using the "Save" button.
	
.. image:: img/scan/scan_select_saved.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
A message appears that it has run successfully.
	
.. image:: img/scan/scan_select_datastore.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
To edit or delete the file, go to :doc:`Datastore<../usage-datastore>` and select "Local". Here you can see the created file. 

.. note::
    The filename is: "profilename_username_timestamp.csv". If more datasets are created with the same profile, username and on the same day, they will all be written to this one file. If the username is changed or the datasets are edited on a different day, a new file will be added automatically. The username can be changed or disabled in :doc:`Settings<../usage-settings>`.

------------

===========
Scan barcode
===========

To create datasets faster during execution, use the scan function to scan barcodes.

.. image:: img/scan/scan_select_overview.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
It is possible to scan everywhere where there is a "barcode" button next to the text field.

.. image:: img/scan/scan_select_scanbutton.jpg
    :alt: alternate text
    :align: center
	
Use the "barcode" button to open the barcode scanner.
	
.. image:: img/app_scan3.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Hold the mobile device to the barcode to capture it.

.. image:: img/scan/scan_select_scan.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
The entered data is transferred to the text field. Click on the "Save" button, to save the dataset.

------------

------------

.. _Datastore Overview:

*********
Datastore
*********

===========
Local
===========

In Local, individual datasets of the saved files can be edited or deleted. At least one file must have been created before this function can be used. If all existing datasets are deleted at once, the file is also disposed of. The files are stored on the local storage and no internet connection is needed for the treatment.   

-----------
Edit datasets
-----------

After a dataset has been created, it is possible to edit it subsequently. 

.. image:: img/1_home.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Click on the "Datastore" button on the home screen.

.. image:: img/3_1_edit.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Now click the "Local" button.

.. image:: img/3_2_edit.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Select the file in which you want to change data.

.. image:: img/3_3_edit.jpg
    :height: 600px
    :alt: alternate text
    :align: center

All data records of this file are displayed in a table. Select a line you want to edit by clicking a checkbox. And confirm by pressing the "Edit" button.

.. note::
    Only one checkbox may be selected for editing.

.. image:: img/3_4_edit.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
In this form you have the possibility to change all fields.

.. image:: img/3_5_edit.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
The change will be applied by clicking the "Update" button.

.. image:: img/3_7_edit.jpg
    :height: 600px
    :alt: alternate text
    :align: center

After confirming, you will be taken back to the selection page.

.. image:: img/3_8_edit.jpg
    :height: 600px
    :alt: alternate text
    :align: center

You can see all the changed records by scrolling to the right in the table.

-----------
Delete datasets
-----------

There is a possibility to delete a single or several datasets.

.. image:: img/3_2_edit.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Select the file in which you want to delete data.

.. image:: img/4_1_delete.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Select a checkbox to delete a dataset from the file. Click "Delete".

.. image:: img/4_3_delete.jpg
    :height: 600px
    :alt: alternate text
    :align: center

The data is deleted and the table is automatically updated.

.. image:: img/4_4_delete.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
To delete multiple datasets click on the respective checkboxes. If you want to delete all of them, click on the checkbox in the header and all existing datasets will be selected.
	
.. image:: img/4_6_delete.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
If all datasets are deleted, the file is also deleted and you return to the Datastore selection page.

.. image:: img/4_7_delete.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Go to local again and press the file selector. The file is no longer present.

------------

===========
Online
===========

In Online, it is possible to load information about individual objects from your CMDB directly into the app using a search function or qr scanner. This information can be used to easily create new datasets of similar objects.

.. note::
    A continuous internet connection is required for this.

-----------
Search in i-doit
-----------

Here you have the possibility to search for single objects in your i-doit CMDB by **object title**, **serial number** or **inventory number**. The attributes of these objects can be loaded into profiles for further processing.

.. image:: img/datastore/datastore_online_idoitobject.jpg
    :alt: alternate text
    :align: center
	
An example with a server object. The object has the following attributes: **object title** *"SRVMUCAPPB01"* and **serial number** *"7581-3460-8768-3380-9459-6665-20"* which we use for the search.
	
.. image:: img/datastore/datastore_online_idoitobject_select.jpg
    :height: 600px
    :alt: alternate text
    :align: center

Click on the "Online" button in "Datastore".
	
.. image:: img/datastore/datastore_online_idoitobject_searchword.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
In the search field, we first search for the object title and enter a partial term. 

.. image:: img/datastore/datastore_online_magnifing_glass.jpg
    :alt: alternate text
    :align: center

Then press the "magnifying glass" button to start the search.
	
.. image:: img/datastore/datastore_online_idoitobject_searchwordfound.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Now all search terms that were found are displayed in a table.

* **Search terms:** Term which matched with the search term.
* **Field:** Whether the found term is an object title, serial number or inventory number.
* **Type:** From which object type the search term comes from.

.. image:: img/datastore/datastore_online_searchbyserial.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Now we search for the object by serial number. We enter "75" as a search term and receive all results in which a "75" occurs.
	
.. image:: img/datastore/datastore_online_searchbyserial_row.jpg
    :alt: alternate text
    :align: center
	
If the search term is matched with a serial number or inventory number, the object title is given above the search term as a heading.

.. note::
    Select an object by clicking on the respective line of the table.
	
.. image:: img/datastore/datastore_online_idoitobject_selectobject.jpg
    :height: 600px
    :alt: alternate text
    :align: center

The object title of the selected object is in the heading. Now select a profile into which the attributes will be loaded.

.. image:: img/datastore/datastore_online_idoitobject_selectobjectform.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
The form is filled with the values of the object from the CMDB. These can now be edited further. It is still possible to use the barcode scanner, e.g. to scan in a different serial number. Then click on "Save" to create a new dataset.
	
.. image:: img/datastore/datastore_online_datasetsaved.jpg
    :height: 600px
    :alt: alternate text
    :align: center

After successful saving, you will be returned to the datastore selection window.

-----------
Scan QR Code
-----------
.. image:: img/datastore/datastore_online_idoitobject.jpg
    :alt: alternate text
    :align: center
	
In the i-doit CMDB, each object contains a unique qr code. This can be attached directly to the hardware component in order to obtain its information more quickly. If this qr code is scanned, it is possible to load its attributes into a profile for further processing.

.. image:: img/datastore/datastore_online_qrcodesearch.jpg
    :height: 600px
    :alt: alternate text
    :align: center

Click on the "Online" button in "Datastore" and you will be redirected to the "Search in i-doit" page.

.. image:: img/datastore/datastore_online_qrcodebutton.jpg
    :alt: alternate text
    :align: center
	
Click on the "QR Code" button and the QR code scanner will open.

.. image:: img/datastore/datastore_online_screenshot_scanit.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Point your smartphone camera to the qr code now. When it is recognized, you will hear a beep and the next page will open automatically.

.. image:: img/datastore/datastore_online_idoitobject_selectobject.jpg
    :height: 600px
    :alt: alternate text
    :align: center

The object title of the scanned object is in the heading. Now select a profile into which the attributes will be loaded.

.. image:: img/datastore/datastore_online_idoitobject_selectobjectform.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
The form is filled with the values of the object from the CMDB. These can now be edited further. It is still possible to use the barcode scanner, e.g. to scan in a different serial number. Then click on "Save" to create a new dataset.

-----------

-----------

.. _Settings Overview:

*********
Settings
*********

===========
General
===========

When datasets are saved to local files the naming convention is: *"profilename_username_timestamp.csv"* or *"profilename_timestamp.csv"*. Depending on whether the username has been enabled for saving or not. 

-----------
Enable username information
-----------
.. image:: img/settings/settings_general_enable.jpg
    :alt: General
	:scale: 40%
	:class: with-shadow
    :align: center
	
If the slider is moved to the right, the username is included in the file name. In this example, "*Operator*" is the username.

.. image:: img/settings/settings_enable_username.jpg
    :alt: General
	:scale: 40%
	:class: with-shadow
    :align: center

-----------
Disable username information
-----------

.. image:: img/settings/settings_general_disable.jpg
    :alt: General
	:scale: 40%
	:class: with-shadow
    :align: center
	
If the slider is moved to the left, the username is not included in the file name.
	
.. image:: img/settings/settings_disable_username.jpg
    :alt: General
	:scale: 40%
	:class: with-shadow
    :align: center

------------

===========
Data & Sync
===========

.. image:: img/settings/settings_data_sync.jpg
    :alt: Data and Sync
	:scale: 40%
    :align: center
	
To send and receive data, the access data from your i-doit is required. Enter your i-doit url and the corresponding apikey in the respective fields.

.. note::
    Once the profiles and lists have been saved locally on the device, it is possible to create datasets without an internet connection. They can be transferred to i-doit when an connection is available again.

------------

===========
CMDB Config
===========

.. image:: img/settings/settings_default_notavailable.jpg
    :alt: CMDB Config
	:scale: 40%
    :align: center

After the app has been installed, no lists are available yet.

.. image:: img/settings/settings_get_lists.jpg
    :alt: CMDB Config
	:scale: 40%
    :align: center

First download all lists under "Dropdown Lists" in :doc:`Sync<../usage-sync>`.

.. note::
    All available attributes for "Default Device Type", "CMDB-Status" and "Default Location Types" are obtained from your i-doit CMDB. If you make changes to these values, we recommend updating the lists in the app.

.. image:: img/settings/settings_cmdb_config.jpg
    :alt: CMDB Config
	:scale: 40%
    :align: center
	
After that all available lists are displayed under CMDB Config.

.. warning::
    It is only possible to create datasets after the lists "ObjectTypes.csv and CMDBStatus.csv" have been downloaded.

-----------
Default Device Type
-----------

.. image:: img/settings/settings_default_device.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
Under "Default Device Type" all object types of your CMDB are provided for selection.

.. image:: img/settings/settings_default_dropdown.jpg
    :alt: CMDB Config
    :align: center

The default value is used for all forms where datasets are created. Like for example in "Scan".

-----------
Default CMDB Status
-----------

.. image:: img/settings/settings_default_cmdb.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
Under "Default CMDB Status" all available CMDB statuses of your CMDB are provided for selection.
	
.. image:: img/settings/settings_default_cmdb_dropdown.jpg
    :alt: CMDB Config
    :align: center

The default value is used for all forms where datasets are created. Like for example in "Scan".

-----------
Supported Location Types
-----------

The list "Supported Location Types" serves as a tool for an autocomplete. This is used in forms for the creation of data sets, e.g. in "Scan". Here all numbers of the required location object types are specified. For example 84 for country. If you enter single letters or words in the text field location, this autocomplete helps you with suitable locations that can be selected. 

.. image:: img/settings/settings_supportedlocations1.jpg
    :alt: CMDB Config
    :align: center
	
After installation these object type ids are automatically set as default. (84 = country, 85 = city, 3 = building, 26 = room, 4 = rack, 75 = blade chassis)
	
.. image:: img/settings/settings_supportedlocations2.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
These location object type ids can be modified individually. Make sure that there is a space after the comma.

.. warning::
    After each change, the list must be downloaded again so that the changes are applied. 
	
.. image:: img/settings/settings_supportedlocations3.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
In the text field of location you can now manage your locations with the help of the autocomplete. The complete path of the respective location is displayed for your overview.

------------

===========
i-doit Search
===========

In the online i-doit search in :doc:`Datastore<../usage-datastore>`, a table with the search hits is displayed. Here you can specify the maximum number of search results. 

.. image:: img/settings/settings_idoit_search.jpg
    :alt: idoit Search
	:scale: 40%
    :align: center

After installation the default value is set to 25. 
	
.. image:: img/settings/settings_idoit_search2.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
In i-doit search, a notice appears if there are more results than the maximum that should be displayed.
