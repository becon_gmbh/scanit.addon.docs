##################
Settings
##################

*********
General
*********

When datasets are saved to local files the naming convention is: *"profilename_username_timestamp.csv"* or *"profilename_timestamp.csv"*. Depending on whether the username has been enabled for saving or not. 

===========
Enable username information
===========
.. image:: img/settings/settings_general_enable.jpg
    :alt: General
	:scale: 40%
	:class: with-shadow
    :align: center
	
If the slider is moved to the right, the username is included in the file name. In this example, "*Operator*" is the username.

.. image:: img/settings/settings_enable_username.jpg
    :alt: General
	:scale: 40%
	:class: with-shadow
    :align: center

===========
Disable username information
===========

.. image:: img/settings/settings_general_disable.jpg
    :alt: General
	:scale: 40%
	:class: with-shadow
    :align: center
	
If the slider is moved to the left, the username is not included in the file name.
	
.. image:: img/settings/settings_disable_username.jpg
    :alt: General
	:scale: 40%
	:class: with-shadow
    :align: center

------------

*********
Data & Sync
*********

.. image:: img/settings/settings_data_sync.jpg
    :alt: Data and Sync
	:scale: 40%
    :align: center
	
To send and receive data, the access data from your i-doit is required. Enter your i-doit url and the corresponding apikey in the respective fields.

.. note::
    Once the profiles and lists have been saved locally on the device, it is possible to create datasets without an internet connection. They can be transferred to i-doit when an connection is available again.

------------

*********
CMDB Config
*********

.. image:: img/settings/settings_default_notavailable.jpg
    :alt: CMDB Config
	:scale: 40%
    :align: center

After the app has been installed, no lists are available yet.

.. image:: img/settings/settings_get_lists.jpg
    :alt: CMDB Config
	:scale: 40%
    :align: center

First download all lists under "Dropdown Lists" in :doc:`Sync<../usage-sync>`.

.. note::
    All available attributes for "Default Device Type", "CMDB-Status" and "Default Location Types" are obtained from your i-doit CMDB. If you make changes to these values, we recommend updating the lists in the app.

.. image:: img/settings/settings_cmdb_config.jpg
    :alt: CMDB Config
	:scale: 40%
    :align: center
	
After that all available lists are displayed under CMDB Config.

.. warning::
    It is only possible to create datasets after the lists "ObjectTypes.csv and CMDBStatus.csv" have been downloaded.

===========
Default Device Type
===========

.. image:: img/settings/settings_default_device.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
Under "Default Device Type" all object types of your CMDB are provided for selection.

.. image:: img/settings/settings_default_dropdown.jpg
    :alt: CMDB Config
    :align: center

The default value is used for all forms where datasets are created. Like for example in "Scan".

===========
Default CMDB Status
===========

.. image:: img/settings/settings_default_cmdb.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
Under "Default CMDB Status" all available CMDB statuses of your CMDB are provided for selection.
	
.. image:: img/settings/settings_default_cmdb_dropdown.jpg
    :alt: CMDB Config
    :align: center

The default value is used for all forms where datasets are created. Like for example in "Scan".

===========
Supported Location Types
===========

The list "Supported Location Types" serves as a tool for an autocomplete. This is used in forms for the creation of data sets, e.g. in "Scan". Here all numbers of the required location object types are specified. For example 84 for country. If you enter single letters or words in the text field location, this autocomplete helps you with suitable locations that can be selected. 

.. image:: img/settings/settings_supportedlocations1.jpg
    :alt: CMDB Config
    :align: center
	
After installation these object type ids are automatically set as default. (84 = country, 85 = city, 3 = building, 26 = room, 4 = rack, 75 = blade chassis)
	
.. image:: img/settings/settings_supportedlocations2.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
These location object type ids can be modified individually. Make sure that there is a space after the comma.

.. warning::
    After each change, the list must be downloaded again so that the changes are applied. 
	
.. image:: img/settings/settings_supportedlocations3.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
In the text field of location you can now manage your locations with the help of the autocomplete. The complete path of the respective location is displayed for your overview.

------------

*********
i-doit Search
*********

In the online i-doit search in :doc:`Datastore<../usage-datastore>`, a table with the search hits is displayed. Here you can specify the maximum number of search results. 

.. image:: img/settings/settings_idoit_search.jpg
    :alt: idoit Search
	:scale: 40%
    :align: center

After installation the default value is set to 25. 
	
.. image:: img/settings/settings_idoit_search2.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
In i-doit search, a notice appears if there are more results than the maximum that should be displayed.
