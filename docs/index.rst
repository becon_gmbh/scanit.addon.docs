##################
Welcome to ScanIT user manuals
##################

The ScanIT add-on adds another data import option to i-doit, which allows you to capture various data from barcodes using a mobile handheld scanner. Either new or existing objects can be created or updated. Before the data is imported into the CMDB, it performs data validation with meaningful feedback. If necessary, the data to be imported can be adapted or corrected directly. This avoids having to correct or delete incorrectly imported values in the CMDB. Entries such as manufacturer, model, serial number, inventory number, location, delivery date or similar can be recorded. The add-on supports internal processes such as inventory, warehousing, relocations, inventory, incoming and outgoing goods, and much more.

------------

This documentation for ScanIT add-on is organized into a couple of sections:

* :ref:`about-docs`
* :ref:`introduction-docs`
* :ref:`gettingstarted-docs`
* :ref:`administration-docs`
* :ref:`usage-docs`


.. _about-docs:

.. toctree::
    :maxdepth: 2
    :caption: About

    about

.. _introduction-docs:

.. toctree::
    :maxdepth: 2
    :caption: Introduction

    architecture
    requirements

.. _gettingstarted-docs:

.. toctree::
    :maxdepth: 2
    :caption: Getting Started

    overview
    firststep
    secondstep
    thirdstep
	
.. _administration-docs:

.. toctree::
    :maxdepth: 2
    :caption: Administration

    installation
    configuration
	
.. _usage-docs:

.. toctree::
   :maxdepth: 2
   :caption: Usage
	
   usage-idoit
   usage-android

##################
License
##################

`becon`_ © 2013-2021 becon GmbH

.. _becon: LICENSE.html