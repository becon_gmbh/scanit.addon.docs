##################
1: Create and deploy profiles for the android app
##################

*********
Create profiles
*********

First of all we need to configure process profiles in i-doit.

.. image:: img/firststeps1.png
    :alt: alternate text
    :align: center

Now an profile must be defined, which serves as a form on the mobile device. The three columns depict the dependency of the fields (category-> attributes) including selected attributes in i-doit. If you choose a category on the left side, this will adjust the middle column.
If you have now selected the required attribute fields, you can also adjust the order (via DragAndDrop) at the end.

.. image:: img/firststeps2.png
    :alt: alternate text
    :align: center

Finally, at least one identification field must be defined. This property is used in i-doit to create or update an object.

*********
Deploy profiles
*********

.. image:: img/firststeps3.png
    :alt: alternate text
    :align: center

If you done your configuration, you can save your profile by using the "Save Profile" button. Click the "Generate CSV" button to deploy the CSV file to the android app.


