##################
Datastore
##################

*********
Local
*********

In Local, individual datasets of the saved files can be edited or deleted. At least one file must have been created before this function can be used. If all existing datasets are deleted at once, the file is also disposed of. The files are stored on the local storage and no internet connection is needed for the treatment.   

===========
Edit datasets
===========

After a dataset has been created, it is possible to edit it subsequently. 

.. image:: img/1_home.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Click on the "Datastore" button on the home screen.

.. image:: img/3_1_edit.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Now click the "Local" button.

.. image:: img/3_2_edit.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Select the file in which you want to change data.

.. image:: img/3_3_edit.jpg
    :height: 600px
    :alt: alternate text
    :align: center

All data records of this file are displayed in a table. Select a line you want to edit by clicking a checkbox. And confirm by pressing the "Edit" button.

.. note::
    Only one checkbox may be selected for editing.

.. image:: img/3_4_edit.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
In this form you have the possibility to change all fields.

.. image:: img/3_5_edit.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
The change will be applied by clicking the "Update" button.

.. image:: img/3_7_edit.jpg
    :height: 600px
    :alt: alternate text
    :align: center

After confirming, you will be taken back to the selection page.

.. image:: img/3_8_edit.jpg
    :height: 600px
    :alt: alternate text
    :align: center

You can see all the changed records by scrolling to the right in the table.

===========
Delete datasets
===========

There is a possibility to delete a single or several datasets.

.. image:: img/3_2_edit.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Select the file in which you want to delete data.

.. image:: img/4_1_delete.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Select a checkbox to delete a dataset from the file. Click "Delete".

.. image:: img/4_3_delete.jpg
    :height: 600px
    :alt: alternate text
    :align: center

The data is deleted and the table is automatically updated.

.. image:: img/4_4_delete.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
To delete multiple datasets click on the respective checkboxes. If you want to delete all of them, click on the checkbox in the header and all existing datasets will be selected.
	
.. image:: img/4_6_delete.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
If all datasets are deleted, the file is also deleted and you return to the Datastore selection page.

.. image:: img/4_7_delete.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Go to local again and press the file selector. The file is no longer present.

------------

*********
Online
*********

In Online, it is possible to load information about individual objects from your CMDB directly into the app using a search function or qr scanner. This information can be used to easily create new datasets of similar objects.

.. note::
    A continuous internet connection is required for this.

===========
Search in i-doit
===========

Here you have the possibility to search for single objects in your i-doit CMDB by **object title**, **serial number** or **inventory number**. The attributes of these objects can be loaded into profiles for further processing.

.. image:: img/datastore/datastore_online_idoitobject.jpg
    :alt: alternate text
    :align: center
	
An example with a server object. The object has the following attributes: **object title** *"SRVMUCAPPB01"* and **serial number** *"7581-3460-8768-3380-9459-6665-20"* which we use for the search.
	
.. image:: img/datastore/datastore_online_idoitobject_select.jpg
    :height: 600px
    :alt: alternate text
    :align: center

Click on the "Online" button in "Datastore".
	
.. image:: img/datastore/datastore_online_idoitobject_searchword.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
In the search field, we first search for the object title and enter a partial term. 

.. image:: img/datastore/datastore_online_magnifing_glass.jpg
    :alt: alternate text
    :align: center

Then press the "magnifying glass" button to start the search.
	
.. image:: img/datastore/datastore_online_idoitobject_searchwordfound.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Now all search terms that were found are displayed in a table.

* **Search terms:** Term which matched with the search term.
* **Field:** Whether the found term is an object title, serial number or inventory number.
* **Type:** From which object type the search term comes from.

.. image:: img/datastore/datastore_online_searchbyserial.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Now we search for the object by serial number. We enter "75" as a search term and receive all results in which a "75" occurs.
	
.. image:: img/datastore/datastore_online_searchbyserial_row.jpg
    :alt: alternate text
    :align: center
	
If the search term is matched with a serial number or inventory number, the object title is given above the search term as a heading.

.. note::
    Select an object by clicking on the respective line of the table.
	
.. image:: img/datastore/datastore_online_idoitobject_selectobject.jpg
    :height: 600px
    :alt: alternate text
    :align: center

The object title of the selected object is in the heading. Now select a profile into which the attributes will be loaded.

.. image:: img/datastore/datastore_online_idoitobject_selectobjectform.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
The form is filled with the values of the object from the CMDB. These can now be edited further. It is still possible to use the barcode scanner, e.g. to scan in a different serial number. Then click on "Save" to create a new dataset.
	
.. image:: img/datastore/datastore_online_datasetsaved.jpg
    :height: 600px
    :alt: alternate text
    :align: center

After successful saving, you will be returned to the datastore selection window.

===========
Scan QR Code
===========
.. image:: img/datastore/datastore_online_idoitobject.jpg
    :alt: alternate text
    :align: center
	
In the i-doit CMDB, each object contains a unique qr code. This can be attached directly to the hardware component in order to obtain its information more quickly. If this qr code is scanned, it is possible to load its attributes into a profile for further processing.

.. image:: img/datastore/datastore_online_qrcodesearch.jpg
    :height: 600px
    :alt: alternate text
    :align: center

Click on the "Online" button in "Datastore" and you will be redirected to the "Search in i-doit" page.

.. image:: img/datastore/datastore_online_qrcodebutton.jpg
    :alt: alternate text
    :align: center
	
Click on the "QR Code" button and the QR code scanner will open.

.. image:: img/datastore/datastore_online_screenshot_scanit.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Point your smartphone camera to the qr code now. When it is recognized, you will hear a beep and the next page will open automatically.

.. image:: img/datastore/datastore_online_idoitobject_selectobject.jpg
    :height: 600px
    :alt: alternate text
    :align: center

The object title of the scanned object is in the heading. Now select a profile into which the attributes will be loaded.

.. image:: img/datastore/datastore_online_idoitobject_selectobjectform.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
The form is filled with the values of the object from the CMDB. These can now be edited further. It is still possible to use the barcode scanner, e.g. to scan in a different serial number. Then click on "Save" to create a new dataset.