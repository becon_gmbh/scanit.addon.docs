##################
Overview
##################

The diagramm explain the way of process

.. image:: img/visualization.png
    :alt: alternate text
    :align: center

Follow the steps...

* :doc:`1: Create and deploy profiles for the android app <../firststep>`
* :doc:`2: Get profiles and start scanning <../secondstep>`
* :doc:`3: Send and import data to i-doit <../thirdstep>`