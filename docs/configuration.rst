##################
Configuration
##################

*********
i-doit
*********

After installing API add-on, you need to:

- generate an apikey
- set apikey
- deactive idoit.login auth
- activate JSON-RPC API

.. image:: img/idoit_api_config.png
    :alt: home screen
    :align: center

*********
Android
*********

After installing ScanIT app, you need to set up your connector information.
Click on "Settings" and configure the i-doit url and apikey information under the item "Data & Sync".

.. image:: img/settings/settings_data_sync.jpg
    :alt: alternate text
    :align: center
