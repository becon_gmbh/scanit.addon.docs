##################
Requirements
##################

*********
i-doit
*********

- at least 1.10 Version
- PHP-Extensions curl need to be installed
- AddOn API need to be installed

*********
Android
*********

- at least android Version 10.0
- tested mobile phones
    - Samsung Galaxy S7, S8, S9