##################
Sync
##################

*********
Get data
*********

Get data means that individual information is obtained from the i-doit CMDB. These specify the way in which further details can be stored. Or this data can also be used as a tool to make work easier. These values must be obtained once, from then on further work is possible without an internet connection. A connection is only required to send the data packages back to the database or to use the online mode in datastore.

===========
Profiles
===========

Profiles are created in i-doit and specify to the user which attributes can be stored in the app and which the CMDB expects in the end. This is how the structure of the various forms is built. In conclusion, this means that at least one profile must be accessible on the mobile device so that datasets can be generated.

.. image:: img/sync/sync_getdata_idoitprofiles.jpg
    :alt: CMDB Config
    :align: center
	
Create your customized profiles in the i-doit ScanIT add-on under the menu item "Export".

.. image:: img/sync/sync_getdata_select.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
Click on "Sync" in the home screen and then on "Get Data".

.. note::
    A continuous internet connection is required for this and make sure that you have entered your i-doit url and apikey under :doc:`Settings<../usage-settings>`.
	
.. image:: img/sync/sync_getdata_selectprofiles.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
At "Profiles From I-Doit" all profiles are shown which are received from CMDB. Select the profiles you want to work with and click "Update". The checkbox in the header selects all files.

.. image:: img/sync/sync_getdata_selectprofiles_saved.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
A message appears that the profiles have been successfully stored in the local memory.

.. note::
    When you make changes to profiles in i-doit, don't forget to update the profiles within the app.

.. image:: img/sync/sync_getdata_selectprofiles_saved_select.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
These profiles can be selected immediately at various points to create datasets.
	
.. image:: img/sync/sync_getdata_selectprofiles_saved_selectprofile.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
The creation of the profiles in i-doit determines the structure of the forms and which values are expected.


------------

===========
Lists 
===========

Lists enable a better workflow during work. With the preloaded lists, dropdown lists or autocomplete text with attributes are provided for preselection. This allows you to create new datasets in a short time.


Object Types and CMDB Status
---------

.. image:: img/sync/lists/sync_getdata_lists_select.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
Select the checkboxes "ObjectTypes.csv" and "CMDBStatus.csv" and load the file via "Update".
	
.. image:: img/sync/lists/sync_getdata_lists_saved.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
After successful download a message appears.
	
.. note::
    These two files are needed so that datasets can be created. If these values are changed in the i-doit CMDB, do not forget to reload the lists.
	
.. image:: img/sync/lists/sync_getdata_lists_cmdbstatus.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
.. image:: img/sync/lists/sync_getdata_lists_devicetype.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
In the forms, dropdown lists are provided for the selection of values from the CMDB. In this case for CMDB status and Device Type.

.. note::
    The value shown first can be changed as default value in :doc:`Settings<../usage-settings>`.

Locations
---------

.. image:: img/sync/lists/sync_getdata_lists_select_location.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center
	
The "Location.csv" can be obtained for input assistance in locations. 
	
.. image:: img/sync/lists/sync_getdata_lists_show_locations.jpg
    :height: 600px
    :alt: CMDB Config
    :align: center

Press the text field for locations. As soon as letters are entered, an autocomplete is created for all location object types where these letters occur.

.. note::
    With "*" all available location object types are displayed. In :doc:`Settings<../usage-settings>` you can specify the ids of the location object types to be retrieved from CMDB.

------------

*********
Send data
*********

After the files with the different datasets are complete, they can be sent back to the CMDB. There a change owner has the possibility to check and accept the data.

Click on "Sync" in the home screen and then on "Send Data".

.. image:: img/sync/sync_senddata_select.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
Select the files to be sent to the database. The checkbox in the header selects all files. Press "Send To I-Doit" to start the transmission.
	
.. image:: img/sync/sync_senddata_synced.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
It is displayed whether the respective file was successfully synchronized with the CMDB.
	
.. image:: img/sync/sync_senddata_exists.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
If there is a file with the same name in the CMDB import, it will be mentioned. 
	
.. image:: img/sync/sync_senddata_rest.jpg
    :height: 600px
    :alt: alternate text
    :align: center
	
The data sent to i-doit will be automatically removed from the device and from the list.
	
.. image:: img/sync/sync_senddata_idoit.jpg
    :alt: alternate text
    :align: center
	
After successful transfer, the data supplied can be processed by the change owner under "Import" in i-doit.
